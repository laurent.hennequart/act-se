### Auteurs : Stéphane Devernay, Laurent Hennequart

# SUJET ABORDE : linux et ligne de commande

# OBJECTIFS : découvrir l'arborescence d'un pc par la ligne de commande

# PRE-REQUIS : connaissances de l'arborescence Windows

# PREPARATION : ubuntu virtualisé sur windows 10

# ELEMENTS DE COURS : arborescence, racine, chemins, dossier, fichier, commandes cd, ls, mkdir ... 

# SEANCE PRATIQUE :

## Mise en route

Démarrer le PC puis lancer VirtualBox et démarrer ubuntu.

Lancer le Terminal (ne pas faire ça si vous êtes nés avant 1980).

Tapez `pwd` pour savoir où vous trouvez dans l'arborescence (**dossier courant**).

## Contenu d'un dossier

Taper `ls` pour afficher le contenu du dossier courrant.

Vérifier que le dossier `Documents` est bien présent dans la liste du contenu.

Taper `ls -l` pour avoir une liste détaillée du contenu du dossier courant.

## Se déplacer dans l'arborescence

Le dossier courant est celui de **l'utilisateur**.

Taper `cd Documents` : vous vous retrouvez dans le dossier `Documents`.

Taper `ls -l` et observer.

Taper `cd ..` pour revenir en arrière, on revient dans le dossier **parent**.

Taper `cd /` pour aller à la **racine (`root') du système.

Taper `ls -l` : on a l'ensemble des dossiers et fichiers non-cachés du système.

Taper `cd home` puis `ls`, le dossier `home` contient votre dossier utilisateur appelé `nom_utilisateur` (à changer suivant les machines)

Pour revenir à votre dossier utilisateur : soit 'cd nom_utilisateur' soit si on revient à la racine avec `cd /`, tapez le chemin complet `cd /home/nom_utilisateur`.

## Créer ses propres dossiers 

A partir de votre dossier `nom_utilisateur`, se placer dans le dossier `Documents`.

Taper 'ls' et noter que pour le moment, il n'y a rien.

Taper 'mkdir TP1' puis `ls` : vous avez créé un dossier `TP1`.

Créer une arborescence avec trois dossiers `TP1` `TP2` et `TP3`. Dans chacun de ces dossiers, créer 2 sous-dossiers `AFaire` et `Final`.

Pour supprimer un dossier **vide**, utiliser la commande `rmdir`.

Placer vous dans le dossier `Documents` et taper `rmdir TP3` : vous avez un message indiquant que l'opération n'est pas valide car le dossier n'est pas vide

Effacer les sous-dossiers (vides) de `TP3` puis taper `TP3`.

**Remarque : contrairement à certains systèmes qui demandent une confirmation, linux effectue la suppression sans se poser de question. **

## Gestion des fichiers

Pour créer un fichier texte, on utilise la commande 'touch'.

Se placer dans le dossier `Documents` et taper `touch fichier1.txt`.

Créer de même `fichier2.txt` et `fichier3.txt`. Visualiser le contenu de votre dossier.

Pour supprimer un fichier, commande `rm`. 

Taper `rm fichier3.txt`. Visualiser le contenu de votre dossier.

**Remarque : comme avec `rmdir`, `rm` ne demande aucune confirmation, donc il faut bien réfléchir avant son utilisation et encore plus pour ne pas être amener à utiliser ces deux commande. **

Pour copier un fichier, commande `cp`.

Taper `cp fichier1.txt fichier1bis.txt` et visualiser le contenu de votre dossier.

Taper `cp fichier1.txt fichier1.txt` : remarques ?

Taper `cp fichier1.txt ./TP1/AFaire/fichier.txt` puis vérifier que `fichier1.txt` est bien présent dans le dossier `AFaire`.

Pour copier tous les fichiers du dossier courant vers un dossier : `cp * /chemin`.

Depuis `Document`, taper `cp * ./TP1/Final/` et vérifier que tous les fichiers sont bien dans le dossier `Final`.

## Pour la prochaine séance

### Commencer à établir un memento des commandes linux
### Chercher comment modifier un fichier `.txt` comme ceux créés dans le TP1